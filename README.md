# MI Framework Launcher #

Interface to launch Swift/Objective-C projects automatically without even adding a single line of code. The class initialized with this framework will get loaded even before Application's main class.

### Requirements ###

This Library is almost compatible with all iOS & OS X versions. Can be used in all kinds of project & framework.

### Usage ###

* Drag and drop this framework into your project (Check "Copy items if needed")
* Add the class name(with out extension), which you want to get initiated when your application is loaded, in info.plist (MIFrameworkLauncher.framework/info.plist) for key 'Initializer class'

Note: If you get a crash "dyld: Library not loaded:", Make sure to add this framework under 'Embedded Binaries' in your project General settings.

### Author ###

* [Mohamed Irshad](https://in.linkedin.com/in/irshad365)

### Licence ###

Copyright (c) 2015 Mohamed Irshad

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.