//
//  MILauncher.cpp
//  MIFrameworkLauncher
//
//  Created by Irshad on 11/9/15.
//  
//  https://bitbucket.org/mohamed_irshad/mifameworklauncher

#include <stdio.h>

void initializeDynamicLibrary();


__attribute__((constructor))
static void initialize_QXDynamicLib() {
    printf("__MIFrameworkLauncher__");
    initializeDynamicLibrary();
}