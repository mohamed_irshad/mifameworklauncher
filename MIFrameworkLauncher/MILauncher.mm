//
//  MILauncher.m
//  MIFrameworkLauncher
//
//  Created by Irshad on 11/9/15.
//  
//  https://bitbucket.org/mohamed_irshad/mifameworklauncher

#import <Foundation/Foundation.h>

#define INIT_CLASS @"Initializer class"


@interface MILauncher : NSObject

@end

@implementation MILauncher

void initializeDynamicLibrary() {
    
    NSString* initClassName = [[NSBundle bundleForClass:[MILauncher class]] infoDictionary][INIT_CLASS];
    if (initClassName) {
        id classObject = NSClassFromString(initClassName);
        if (classObject) {
            NSLog(@"%@ - Intialized", [[classObject alloc]init]);
        }
    }
}

@end
